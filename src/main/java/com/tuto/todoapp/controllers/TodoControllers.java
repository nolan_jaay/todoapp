package com.tuto.todoapp.controllers;

import com.tuto.todoapp.entities.Todo;
import com.tuto.todoapp.responses.CustomResponse;
import com.tuto.todoapp.services.interfaces.TodoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class TodoControllers {

    private TodoService todoService;

    public TodoControllers(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todos")
    public ResponseEntity<CustomResponse> getAllTodos(){
        return todoService.getAllTodos();
    }

    @GetMapping("/todosById/{id}")
    public ResponseEntity<CustomResponse> findTodoById(@PathVariable Long id){
        return todoService.findTodoById(id);
    }

    @GetMapping("/todosByStatus/{status}")
    public ResponseEntity<CustomResponse> findTodoByStatus(@PathVariable boolean status){
        return todoService.findTodoByStatus(status);
    }

    @PutMapping("/todos/{id}")
    public ResponseEntity<CustomResponse> updateTodo(@PathVariable Long id,@RequestBody Todo todo){
        return todoService.updateTodo(id, todo);
    }

    @PostMapping("/todos")
    public ResponseEntity<CustomResponse> addTodo(@RequestBody Todo todo){
        return todoService.addTodo(todo);
    }

    @DeleteMapping("/todos/{id}")
    public ResponseEntity<CustomResponse> deleteTodo(@PathVariable Long id){
        return todoService.deleteTodo(id);
    }
}
