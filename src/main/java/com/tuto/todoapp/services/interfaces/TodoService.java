package com.tuto.todoapp.services.interfaces;

import com.tuto.todoapp.entities.Todo;
import com.tuto.todoapp.responses.CustomResponse;
import org.springframework.http.ResponseEntity;

public interface TodoService {
    public ResponseEntity<CustomResponse> getAllTodos();
    public ResponseEntity<CustomResponse> findTodoById(Long id);
    public ResponseEntity<CustomResponse> findTodoByStatus(boolean status);
    public ResponseEntity<CustomResponse> updateTodo(Long id, Todo todo);
    public ResponseEntity<CustomResponse> addTodo(Todo todo);
    public ResponseEntity<CustomResponse> deleteTodo(Long id);

}
