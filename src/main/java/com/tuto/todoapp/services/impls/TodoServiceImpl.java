package com.tuto.todoapp.services.impls;

import com.tuto.todoapp.entities.Todo;
import com.tuto.todoapp.repositories.TodoRepository;
import com.tuto.todoapp.responses.CustomResponse;
import com.tuto.todoapp.services.interfaces.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class TodoServiceImpl implements TodoService {
    private TodoRepository todoRepository;

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public ResponseEntity<CustomResponse> getAllTodos() {
        try{
            return new ResponseEntity<>(new CustomResponse(200, "listes des tâches à faire", todoRepository.findAll()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de la récupération des listes de tâches à faire", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomResponse> findTodoById(Long id) {
        try{
            if(todoRepository.existsById(id)){
                Todo todo = todoRepository.findById(id).get();
                return new ResponseEntity<>(new CustomResponse(200, "Tâche avec id "+id+" retrouvée avec succès", Arrays.asList(todo)), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(new CustomResponse(400, "Aucune tâche avec l'id "+id, null), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de la récupération de la tâches avec id "+id, null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomResponse> findTodoByStatus(boolean status) {
        try{
            List<Todo> todos = todoRepository.findTodoByComplet(status);
            return new ResponseEntity<>(new CustomResponse(200, "Liste des tâches récupérées avec succès", todos), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de la récupération de la liste des tâches", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomResponse> updateTodo(Long id, Todo todo) {
        try{
            ResponseEntity<CustomResponse> responseEntity = findTodoById(id);
            Todo todoToUpdate = responseEntity.getBody().getObjects().get(0);
            todoToUpdate.setComplet(todo.isComplet());
            todoToUpdate.setLibelle(todo.getLibelle());
            Todo savedTodo = todoRepository.save(todoToUpdate);
            return new ResponseEntity<>(new CustomResponse(200, "Tâches modifiée avec succès", Arrays.asList(savedTodo)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de la modification de la tâche", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomResponse> addTodo(Todo todo) {
        try{
            Todo savedTodo = todoRepository.save(todo);
            return new ResponseEntity<>(new CustomResponse(200, "Tâches ajoutée avec succès", Arrays.asList(savedTodo)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de l'ajout de la tâche", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<CustomResponse> deleteTodo(Long id) {
        try{
            todoRepository.deleteById(id);
            return new ResponseEntity<>(new CustomResponse(200, "Tâches supprimée avec succès", null), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(new CustomResponse(500, "Erreur lors de la suppression de la tâche", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
