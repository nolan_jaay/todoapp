package com.tuto.todoapp.responses;

import com.tuto.todoapp.entities.Todo;

import java.util.List;

public class CustomResponse {
    private Integer status;
    private String message;
    private List<Todo> objects;

    public CustomResponse(Integer status, String message, List<Todo> objects) {
        this.status = status;
        this.message = message;
        this.objects = objects;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Todo> getObjects() {
        return objects;
    }

    public void setObjects(List<Todo> objects) {
        this.objects = objects;
    }
}
