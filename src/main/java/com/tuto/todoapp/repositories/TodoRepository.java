package com.tuto.todoapp.repositories;

import com.tuto.todoapp.entities.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, Long> {

    public List<Todo> findTodoByComplet(boolean complet);
}
